#include <NewPing.h>

#define MOTOR1_A 6
#define MOTOR1   7
#define MOTOR2_A 8
#define MOTOR2   9
#define LED      13
#define ENA      11
#define ENB      10

#define TRIGGER_A 4
#define ECHO_A    5

#define TRIGGER_C 2
#define ECHO_C    3

float distLateral = 0.0;

NewPing sonarLateral(TRIGGER_C, ECHO_C, 20);
NewPing sonarFrontal(TRIGGER_A, ECHO_A, 20);

//Funcioes Motores
void mot_L (int a, int b){
  //motor1 sentido vel
  analogWrite (ENA, b);
  if (a==0) {
    digitalWrite(MOTOR1,HIGH);
    digitalWrite(MOTOR1_A,LOW);
  }
  else {
    digitalWrite(MOTOR1,LOW);
    digitalWrite(MOTOR1_A,HIGH);
  }
}

void mot_R (int c, int d){
  //motor1 sentido vel
  analogWrite (ENB, d);
  if (c==0) {
    digitalWrite(MOTOR2,HIGH);
    digitalWrite(MOTOR2_A,LOW);
  }
  else {
    digitalWrite(MOTOR2,LOW);
    digitalWrite(MOTOR2_A,HIGH);
  }
}

void setup() {
  
  pinMode(MOTOR1,   OUTPUT);
  pinMode(MOTOR2,   OUTPUT);
  pinMode(MOTOR1_A, OUTPUT);
  pinMode(MOTOR2_A, OUTPUT);  
  pinMode(ENB,      OUTPUT);
  pinMode(ENA,      OUTPUT);

  Serial.begin(9600);
}

void loop() {

//  distLateral = sonarLateral.convert_cm(sonarLateral.ping_median(7));
  
  if (sonarFrontal.ping_cm() < 12) {
    if (sonarLateral.ping_cm() < 12) {
      mot_L(0, 0);
      mot_R(0, 255);
      delay(5);
    } else {
      mot_L(0, 255);
      mot_R(0, 255);
      delay(5);
    }
  } else {
    mot_L(0, 255);
    mot_R(0, 255);
  }

  // Em caso de emergência, dar ré
  if (sonarFrontal.ping_cm() <= 2) {
    mot_L(1, 200);
    mot_R(1, 200);
    delay(10);
  }
  
}
